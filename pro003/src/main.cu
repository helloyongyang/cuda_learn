#include <stdio.h>

__global__ void hello_world_1x1(void)
{
    printf("GPU: Hello world!\n");
    const int block_id = blockIdx.x;
    const int thread_id = threadIdx.x;
    const int id = block_id * blockDim.x + thread_id;
    printf("Block ID: %d, Thread ID: %d, ID: %d\n", block_id, thread_id, id);
}

__global__ void hello_world_2x2(void)
{
    printf("GPU: Hello world!\n");
    const int block_id = blockIdx.y * gridDim.x + blockIdx.x;
    const int thread_id = threadIdx.y * blockDim.x + threadIdx.x;
    const int id = block_id * (blockDim.x * blockDim.y) + thread_id;
    printf("Block ID: %d, Thread ID: %d, ID: %d\n", block_id, thread_id, id);
}

__global__ void hello_world_3x3(void)
{
    printf("GPU: Hello world!\n");
    const int block_id = blockIdx.z * (gridDim.y * gridDim.x) + blockIdx.y * gridDim.x + blockIdx.x;
    const int thread_id = threadIdx.z * (blockDim.y * blockDim.x) + threadIdx.y * blockDim.x + threadIdx.x;
    const int id = block_id * (blockDim.x * blockDim.y * blockDim.z) + thread_id;
    printf("Block ID: %d, Thread ID: %d, ID: %d\n", block_id, thread_id, id);
}

int main(int argc, char **argv)
{
    printf("CPU: Hello world!\n");
    dim3 grid_size_1x1(2);
    dim3 block_size_1x1(4);
    hello_world_1x1<<<grid_size_1x1, block_size_1x1>>>();
    cudaDeviceSynchronize();

    dim3 grid_size_2x2(2, 3);
    dim3 block_size_2x2(4, 5);
    hello_world_2x2<<<grid_size_2x2, block_size_2x2>>>();
    cudaDeviceSynchronize();

    dim3 grid_size_3x3(2, 3, 4);
    dim3 block_size_3x3(4, 5, 6);
    hello_world_3x3<<<grid_size_3x3, block_size_3x3>>>();
    cudaDeviceSynchronize();

    return 0;
}
