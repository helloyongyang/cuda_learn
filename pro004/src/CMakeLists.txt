set(EXECUTABLE_OUTPUT_PATH ${PROJECT_BINARY_DIR}/bin)

enable_language(CUDA)

set(CMAKE_CUDA_ARCHITECTURES 80)

add_executable(main main.cu)
