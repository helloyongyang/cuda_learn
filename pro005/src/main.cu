#include <stdio.h>
#include <algorithm>


#define N 1024
#define r 3
#define BLOCK_SIZE 128


__global__ void stencil_1d(int *in, int *out){
    __shared__ int temp[BLOCK_SIZE + 2 * r];//per-block, static allocation
    int gindex = threadIdx.x + blockIdx.x * blockDim.x;//用来索引global memory
    int lindex = threadIdx.x + r;//用来索引shared memory
    
    //把数据从global memory读到shared memory里。没有for循环，SIMT模式，一个block里所有线程合作完成
    temp[lindex] = in[gindex];
    if(threadIdx.x < r){
        temp[lindex - r] = in[gindex - r];
        temp[lindex + BLOCK_SIZE] = in[gindex + BLOCK_SIZE];
    }
    
    __syncthreads();//确保所有数据已经读到shared memory里，注意不能写在if条件语句里
    int res = 0;
    //此时需要for循环，因为一次线程要负责计算2*r+1个元素的和
    for(int offset = -r; offset <= r;offset++)
        res += temp[lindex + offset];
    out[gindex] = res;
}


int main(int argc, char **argv)
{
    int *in, *out;
    int *d_in, *d_out;

    int size = (N + 2 * r) * sizeof(int);

    in = (int *)malloc(size);
    std::fill_n(in, N + 2 * r, 1);

    out = (int *)malloc(size);
    std::fill_n(out, N + 2 * r, 1);

    cudaMalloc((void **)&d_in, size);
    cudaMalloc((void **)&d_out, size);

    cudaMemcpy(d_in, in, size, cudaMemcpyHostToDevice);
    cudaMemcpy(d_out, out, size, cudaMemcpyHostToDevice);

    stencil_1d<<<(N + BLOCK_SIZE - 1) / BLOCK_SIZE, BLOCK_SIZE>>>(d_in + r, d_out + r);

    cudaMemcpy(out, d_out, size, cudaMemcpyDeviceToHost);

    for (int i = 0; i < N + 2 * r; i++) {
        if (i < r || i >= N +r) {
            if (out[i] != 1) {
                printf("Mismatch at index %d, was: %d, should be: %d\n", i, out[i], 1);
            }
        }
        else {
            if (out[i] != 2 * r + 1) {
                printf("Mismatch at index %d, was: %d, should be: %d\n", i, out[i], 2 * r + 1);
            }
        }
    }

    free(in);
    free(out);
    cudaFree(d_in);
    cudaFree(d_out);
    printf("Sucess!\n");

    return 0;
}
