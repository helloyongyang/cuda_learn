#include <stdio.h>

__global__ void hello_world(void)
{
    printf("GPU: Hello world!\n");
    const int block_id = blockIdx.x;
    const int thread_id = threadIdx.x;
    const int id = threadIdx.x + blockIdx.x * blockDim.x;
    printf("Block ID: %d, Thread ID: %d, ID: %d\n", block_id, thread_id, id);
}

int main(int argc, char **argv)
{
    printf("CPU: Hello world!\n");
    hello_world<<<2, 4>>>();
    cudaDeviceSynchronize();
    return 0;
}
