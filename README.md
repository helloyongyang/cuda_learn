# cuda_learn

这是雍洋的自学cuda编程的项目

进入每个子项目之后，分别执行
```
mkdir build
cd build
cmake ..
make
```
即可在`build/bin`文件夹下找到编译好的可执行程序

子项目列表：
- pro001: 一个简单的gpu程序的cmake工程，可以作为一个模板
- pro002: grid和block的设置
- pro003: 多维grid和多维block的设置
- pro004: 向量加法
- pro005: 共享内存
- pro006: 获取GPU信息
- pro007: 一个kernel的并行度上限测试
